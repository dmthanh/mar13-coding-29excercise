package ex6

import (
	"fmt"
	"testing"
)

func TestEx6(t *testing.T) {
	var result int
	testcase := make([][][]string, 0)
	answer := make([]int, 0)

	testcase = append(testcase, [][]string{
		[]string{
			"codility1",
			"codility3",
			"codility2",
			"codility4b",
			"codility4a"},
		[]string{
			"Wrong answer",
			"OK",
			"OK",
			"Runtime error",
			"OK"}})

	answer = append(answer, 50)

	testcase = append(testcase, [][]string{
		[]string{
			"test1",
			"test2",
			"test3"},
		[]string{
			"Wrong answer",
			"Runtime error",
			"OK"}})
	answer = append(answer, 33)

	testcase = append(testcase, [][]string{
		[]string{
			"test1",
			"test2",
			"test3a",
			"test3b"},
		[]string{
			"OK",
			"OK",
			"OK",
			"Runtime error"}})
	answer = append(answer, 66)


	for i := range testcase {
		result = Ex6(testcase[i][0], testcase[i][1])
		if result != answer[i] {
			t.Error("Fail case/correct/your answer", testcase[i], answer[i], result)
		} else {
			fmt.Println("test case: ", testcase[i])
			fmt.Println("answer: ", answer[i])
		}
	}
}
