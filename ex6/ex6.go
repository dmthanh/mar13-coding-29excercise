package ex6

import (
	"strconv"
)

func findTestNameLength(s string) int {
	for i := len(s) - 2; i > 0; i-- {
		_, err := strconv.Atoi(string(s[i]))
		if err != nil {
			return i
		}
	}
	return 0
}
func Ex6(t, r []string) int {
	le := findTestNameLength(t[0])
	max := 0
	var check [300]bool
	var k int
	var err error
	for i := range t {
		k, err = strconv.Atoi(string(t[i][le+1:]))
		if err != nil {
			k, _ = strconv.Atoi(string(t[i][le+1 : len(t[i])-1]))
		}
		if k > max {
			max = k
		}
		if r[i] != "OK" {
			check[k-1] = true
		}
	}
	k = 0
	for i := 0; i < max; i++ {
		if !check[i] {
			k++
		}
	}
	return int(float32(k) / float32(max) * 100)
}
