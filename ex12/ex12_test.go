package ex12

import "testing"

func TestEx12(t *testing.T) {
	var result int

	var testcase []string
	var answer []int

	testcase = append(testcase, "We test coders. Give us a try?")
	answer = append(answer, 4)

	testcase = append(testcase, "Forget CVs..Save time . x x")
	answer = append(answer, 2)

	testcase = append(testcase, "Mot hai ba bon ngay..Tu khi quen em . Anh da biet boi roi")
	answer = append(answer, 5)

	for i := range answer {
		result = Ex12(testcase[i])
		if result != answer[i] {
			t.Error("Fail case/correct/your answer", testcase[i], answer[i], result)
		}
	}
}
