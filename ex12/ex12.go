package ex12

import "strings"

func Ex12(s string) int {
	s = strings.ReplaceAll(s, "?", ".")
	s = strings.ReplaceAll(s, "!", ".")
	sentence := strings.Split(s, ".")
	maxLen := 0
	var tmp []string
	for _, v := range sentence {
		tmp = strings.Fields(v)
		if len(tmp) > maxLen {
			maxLen = len(tmp)
		}
	}
	return maxLen
}
