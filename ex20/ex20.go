package ex20

func palindromic(a string) bool {
	for i := 0; i < len(a)/2; i++ {
		if a[i] != a[len(a)-i-1] {
			return false
		}
	}
	return true
}
func Ex20(a string) string {
	maxSubstring := ""
	for i := 0; i < len(a); i++ {
		for j := len(a) - 1; j >= i; j-- {
			if a[j] == a[i] {
				if palindromic(a[i:j+1]) && len(a[i:j+1]) > len(maxSubstring) {
					maxSubstring = a[i : j+1]
				}
			}
		}
	}
	return maxSubstring
}
