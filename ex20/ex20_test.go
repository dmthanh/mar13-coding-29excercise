package ex20

import (
	"fmt"
	"testing"
)

func TestEx20(t *testing.T) {
	var result string
	var testcase []string
	var answer []string

	testcase = append(testcase, "babad")
	answer = append(answer, "bab")
	testcase = append(testcase, "cbbd")
	answer = append(answer, "bb")
	testcase = append(testcase, "aaaadddd")
	answer = append(answer, "aaaa")
	for i := range answer {
		result = Ex20(testcase[i])
		if result != answer[i] {
			t.Error("Fail case/correct/your answer", testcase[i], answer[i], result)
		} else {
			fmt.Println("test case: ", testcase[i])
			fmt.Println("answer: ", answer[i])
			fmt.Println("result: ", result)
		}
	}
}
