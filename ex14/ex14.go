package ex14

import "math"

func Ex14(a int) int {
	var end []int
	var b int
	if a < 0 {
		b = -1 * a
	} else {
		b = a
	}
	for b > 0 {
		end = append(end, b%10)
		b = b / 10
	}
	b = 0
	for i, v := range end {
		b += v * int(math.Pow10(len(end)-i-1))
	}
	if a < 0 {
		return -1 * b
	} else {
		return b
	}
}
