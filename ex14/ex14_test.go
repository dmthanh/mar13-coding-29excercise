package ex14

import (
	"fmt"
	"testing"
)

//excercise 1 in file #2
func TestEx14(t *testing.T) {
	var result int
	var testcase []int
	var answer []int

	testcase = append(testcase, 123)
	answer = append(answer, 321)
	testcase = append(testcase, -123)
	answer = append(answer, -321)
	testcase = append(testcase, 120)
	answer = append(answer, 21)
	testcase = append(testcase, 100)
	answer = append(answer, 1)
	testcase = append(testcase, 1)
	answer = append(answer, 1)

	for i := range answer {
		result = Ex14(testcase[i])
		if result != answer[i] {
			t.Error("Fail case/correct/your answer", testcase[i], answer[i], result)
		} else {
			fmt.Println("test case: ", testcase[i])
			fmt.Println("answer: ", answer[i])
			fmt.Println("result: ", result)
		}
	}
}
