package ex17

func Ex17(s []string) string {
	tmp := ""
	for j := 0; j < len(s[0]); j++ {
		for i := 1; i < len(s); i++ {
			if s[0][j] != s[i][j] {
				return tmp
			}

		}
		tmp += string(s[0][j])
	}
	return tmp
}
