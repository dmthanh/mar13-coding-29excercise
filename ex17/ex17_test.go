package ex17

import (
	"fmt"
	"testing"
)

//excercise 1 in file #2
func TestEx17(t *testing.T) {
	var result string
	var testcase [][]string
	var answer []string

	testcase = append(testcase, []string{"flower", "flow", "flight"})
	answer = append(answer, "fl")
	testcase = append(testcase, []string{"dog", "racecar", "car"})
	answer = append(answer, "")
	testcase = append(testcase, []string{"dog", "dover", "do"})
	answer = append(answer, "do")
	testcase = append(testcase, []string{"dog", "dover", "od"})
	answer = append(answer, "")

	for i := range answer {
		result = Ex17(testcase[i])
		if result != answer[i] {
			t.Error("Fail case/correct/your answer", testcase[i], answer[i], result)
		}  else {
			fmt.Println("test case: ", testcase[i])
			fmt.Println("answer: ", answer[i])
			fmt.Println("result: ", result)
		}
	}
}
