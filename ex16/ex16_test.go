package ex16

import (
	"fmt"
	"testing"
)

//excercise 1 in file #2
func TestEx16(t *testing.T) {
	var result int
	var testcase []string
	var answer []int

	testcase = append(testcase, "III")
	answer = append(answer, 3)
	testcase = append(testcase, "IV")
	answer = append(answer, 4)
	testcase = append(testcase, "IX")
	answer = append(answer, 9)
	testcase = append(testcase, "LVIII")
	answer = append(answer, 58)
	testcase = append(testcase, "MCMXCIV")
	answer = append(answer, 1994)
	testcase = append(testcase, "XL")
	answer = append(answer, 40)
	testcase = append(testcase, "XC")
	answer = append(answer, 90)
	testcase = append(testcase, "XLI")
	answer = append(answer, 41)


	for i := range answer {
		result = Ex16(testcase[i])
		if result != answer[i] {
			t.Error("Fail case/correct/your answer", testcase[i], answer[i], result)
		} else {
			fmt.Println("test case: ", testcase[i])
			fmt.Println("answer: ", answer[i])
			fmt.Println("result: ", result)
		}
	}
}
