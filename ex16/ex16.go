package ex16

func Ex16(s string) int {
	var toInt = map[byte]int{
		'I': 1,
		'V': 5,
		'X': 10,
		'L': 50,
		'C': 100,
		'D': 500,
		'M': 1000,
	}
	tmp := 0
	for i := range s {
		if i < len(s)-1 {
			if toInt[s[i]] < toInt[s[i+1]] {
				tmp -= toInt[s[i]]
			} else {
				tmp += toInt[s[i]]
			}
		} else {
			tmp += toInt[s[i]]
		}
	}
	return tmp
}
