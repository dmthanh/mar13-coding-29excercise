package ex9

import (
	"fmt"
	"testing"
)

func TestEx9(t *testing.T) {
	var result int
	type input struct {
		y       int
		a, b, w string
	}
	var testcase []input
	var answer []int

	testcase = append(testcase, input{
		2014,
		"April",
		"May",
		"Wednesday"})
	answer = append(answer, 7)

	testcase = append(testcase, input{
		2021, "January", "February", "Friday"})
	answer = append(answer, 7)

	testcase = append(testcase, input{
		2018, "May", "June", "Monday"})
	answer = append(answer, 7)
	testcase = append(testcase, input{
		2019, "January", "February", "Tuesday"})
	answer = append(answer, 7)
	testcase = append(testcase, input{
		2020, "January", "February", "Wednesday"})
	answer = append(answer, 7)


	for i := range answer {
		result = Ex9(testcase[i].y, testcase[i].a, testcase[i].b, testcase[i].w)
		res :=Solution(testcase[i].y, testcase[i].a, testcase[i].b, testcase[i].w)
		//if result != answer[i] {
		//	t.Error("Fail case/correct/your answer", testcase[i], answer[i], result)
		//} else {
		//	fmt.Println("test case: ", testcase[i])
		//	fmt.Println("answer: ", answer[i])
		//	fmt.Println("result: ", result)
		//}
		fmt.Println(result," ",res)

	}

}
