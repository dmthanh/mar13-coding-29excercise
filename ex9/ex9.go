package ex9

func isLeapYear(y int) bool {
	if y%4 == 0 {
		return true
	}
	return false
}

var dayToInt = map[string]int{
	"Monday":    0,
	"Tuesday":   1,
	"Wednesday": 2,
	"Thursday":  3,
	"Friday":    4,
	"Saturday":  5,
	"Sunday":    6,
}
var monthToInt = map[string]int{
	"January":   0,
	"February":  1,
	"March":     2,
	"April":     3,
	"May":       4,
	"June":      5,
	"July":      6,
	"August":    7,
	"September": 8,
	"October":   9,
	"November":  10,
	"December":  11,
}
var totalDayInMonth = [...]int{31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}

func Ex9(y int, a, b, w string) int {
	dayOfStartYear := dayToInt[w]
	var startMonth = monthToInt[a]
	endMonth := monthToInt[b]

	if isLeapYear(y) {
		totalDayInMonth[1] = 29
	}
	day := dayOfStartYear
	count := 0
	for month := 0; month <= endMonth; month++ {
		for date := 0; date < totalDayInMonth[month]; date++ {
			if startMonth <= month && month <= endMonth && day == 0 {
				count++
				if month == endMonth && date+6 >= totalDayInMonth[month] {
					count--
				}
			}
			day = (day + 1) % 7
		}
	}
	return count
}
