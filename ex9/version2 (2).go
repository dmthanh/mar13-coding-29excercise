package ex9

import (
	"fmt"
	"strings"
)

//Global variables
var (
	//The array of strings contains the names of months within a year in order
	MonthsInYear = []string{"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"}

	//The array of strings contains the names of days within a week in order
	DaysInWeek = []string{"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"}

	//The arrays of integers contain the number of days of each month in a year
	//The only difference is February has 29 days in Leap Year, and only 28 days in Normal Year
	DaysInNormalYear = []int{31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}
	DaysInLeapYear = []int{31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}
)

//Main solution
func Solution(Y int, A string, B string, W string) int {
	var count int

	//The indexes of Begin month and End month in array MonthsInYear
	beginIndex, endIndex := GetIndexOfMonthsFromString(A, B)

	//The index of January 1st in array DaysInWeek
	firstDayIndex := GetIndexOfFirstDayFromString(W)

	//Calculate the weeks with condition of leap year or normal year
	if IsLeapYear(Y) {
		count = CountWeeks(beginIndex, endIndex, firstDayIndex, DaysInLeapYear)
	} else {
		count = CountWeeks(beginIndex, endIndex, firstDayIndex, DaysInNormalYear)
	}
	return count
}

//In the range 2001-2099, if the year is divisible for 4, it's a leap year
func IsLeapYear(Y int) bool {
	if Y % 4 == 0 {
		return true
	}
	return false
}

//Get the indexes of begin month and end month in the MonthsInYear array
func GetIndexOfMonthsFromString(A string, B string) (int, int){
	var beginIndex, endIndex int

	for index, month := range MonthsInYear {
		if strings.Compare(A, month) == 0 {
			beginIndex = index
		}
		if strings.Compare(B, month) == 0 {
			endIndex = index
		}
	}
	return beginIndex, endIndex
}

//Get the index of January 1st in the DaysInWeek array
func GetIndexOfFirstDayFromString(W string) int {
	var firstDayIndex int
	for index, day := range DaysInWeek {
		if strings.Compare(W, day) == 0 {
			firstDayIndex = index
			break
		}
	}
	return firstDayIndex
}

//Count the number of weeks John can stay in Hawaii
func CountWeeks(beginIndex int, endIndex int, firstDayIndex int, DaysInYear []int) int {
	var count int
	var numberOfDaysBegin, numberOfDaysEnd int

	//Calculate the number of days from the begin of the year to the day before the Begin month 1st
	//and number of days from the begin of the year to the ending day of End month
	for index := 0; index <= endIndex; index++ {
		if index < beginIndex {
			numberOfDaysBegin += DaysInYear[index]
		}
		numberOfDaysEnd += DaysInYear[index]
	}

	//Calculate the remainder days to the last Weekday before begin month, which is equal to the week day of January 1st
	leftOfMonthsBegin := (firstDayIndex + numberOfDaysBegin) % 7
	var firstMondayOfBeginMonth int

	//Get the day of the first Monday in begin month
	if leftOfMonthsBegin == 0 {
		firstMondayOfBeginMonth = 1
	} else {
		firstMondayOfBeginMonth = 7 - leftOfMonthsBegin + 1
	}


	//Number of weeks is equal the following division
	count = (numberOfDaysEnd - numberOfDaysBegin - firstMondayOfBeginMonth + 1) / 7
	return count
}

func main() {
	//fmt.Println("The number of weeks John can stay in Hawaii is: ", Solution(2018, "May", "June", "Monday"))
	//fmt.Println(Solution(2021, "January", "February", "Friday"))
	fmt.Println(Solution(2014, "April", "May", "Wednesday"))
}
