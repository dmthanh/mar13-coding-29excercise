package ex5

import (
	"fmt"
	"testing"
)

func TestEx5(t *testing.T) {
	testcase := make([]string, 0)
	answer := make([]string, 0)

	testcase = append(testcase,
		"my.song.mp3 11b\n"+
			"greatSong.flac 1000b\n"+
			"not3.txt 5b\n"+
			"video.mp4 200b\n"+
			"game.exe 100b\n"+
			"mov!e.mkv 10000b")
	answer = append(answer,
		"music 1011b\n"+
			"images 0b\n"+
			"movies 10200b\n"+
			"other 105b")

	testcase = append(testcase,
		"myheart.mp3 1000b\n"+
			"game.exe 100b\n"+
			"mov!e.mkv 10000b")

	answer = append(answer,
		"music 1000b\n"+
			"images 0b\n"+
			"movies 10000b\n"+
			"other 100b")


	var result string
	for i := range testcase {
		result = Ex5(testcase[i])
		if result != answer[i] {
			t.Error("Ex4 fail/testcase/correct answer/your anwser", testcase[i], "\n", answer[i], "\n", result)
		} else {
			fmt.Println("test case: ", testcase[i])
			fmt.Println("answer: ", answer[i])
		}
	}

}
