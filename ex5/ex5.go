package ex5

import (
	"strconv"
	"strings"
)

func toInt(s string) int {
	i, err := strconv.Atoi(s)
	if err == nil {
		return i
	} else {
		return 0
	}
}

func toStr(a int) string {
	return strconv.Itoa(a)
}

func Ex5(s string) string {
	music := "mp3 aac flac"
	image := "jpg bmp gif"
	movie := "mp4 avi mkv"
	rawLines := strings.Split(s, "\n")
	var splitLine []string
	var format string
	var size int
	sMusic, sMovie, sImage, sOther := 0, 0, 0, 0
	for _, line := range rawLines {
		splitLine = strings.Split(line, " ")
		format = splitLine[0][len(splitLine[0])-3:]
		size = toInt(splitLine[1][:len(splitLine[1])-1])
		if strings.Contains(music, format) {
			sMusic += size
		} else {
			if strings.Contains(movie, format) {
				sMovie += size
			} else {
				if strings.Contains(image, format) {
					sImage += size
				} else {
					sOther += size
				}
			}
		}

	}
	result := "music " + toStr(sMusic) + "b\n" +
		"images " + toStr(sImage) + "b\n" +
		"movies " + toStr(sMovie) + "b\n" +
		"other " + toStr(sOther) + "b"
	return result

}
