package ex10

func Ex10(a [][]int) int {
	var top, bottom, left, right int
	count := 0
	for i := range a {
		for j := range a[i] {
			top, bottom, left, right = 0, 0, 0, 0
			for i1 := 0; i1 < i; i1++ {
				for j1 := range a[i1] {
					top += a[i1][j1]
				}
			}
			for i1 := i + 1; i1 < len(a); i1++ {
				for j1 := range a[i1] {
					bottom += a[i1][j1]
				}
			}
			for i1 := range a {
				for j1 := 0; j1 < j; j1++ {
					left += a[i1][j1]
				}
			}
			for i1 := range a {
				for j1 := j + 1; j1 < len(a[i1]); j1++ {
					right += a[i1][j1]
				}
			}
			if top == bottom && left == right {
				count++
			}

		}
	}
	return count
}
