package ex10

import (
"fmt"
)

func Solution(A [][]int) int {
	if len(A) < 1 || len(A[0]) < 1 {
		return 0
	}
	var count int
	for row := 0; row < len(A); row++ {
		for column := 0; column < len(A[row]); column++ {
			if SumAbove(A, row) == SumBelow(A, row) && SumLeft(A, column) == SumRight(A, column) {
				count++
			}
		}
	}
	return count
}

func SumAbove(A [][]int, currentRow int) int {
	var sum int
	for row := 0; row < currentRow; row++ {
		for column := 0; column < len(A[row]); column++ {
			sum += A[row][column]
		}
	}
	return sum
}

func SumBelow(A [][]int, currentRow int) int {
	var sum int
	for row := currentRow + 1; row < len(A); row++ {
		for column := 0; column < len(A[row]); column++ {
			sum += A[row][column]
		}
	}
	return sum
}

func SumLeft(A [][]int, currentColumn int) int {
	var sum int
	for row := 0; row < len(A); row++ {
		for column := 0; column < currentColumn; column++ {
			sum += A[row][column]
		}
	}
	return sum
}

func SumRight(A [][]int, currentColumn int) int {
	var sum int
	for row := 0; row < len(A); row++ {
		for column := currentColumn + 1; column < len(A[row]); column++ {
			sum += A[row][column]
		}
	}
	return sum
}

func main() {
	fmt.Println(Solution([][]int{
		{2,7,5},
		{3,1,1},
		{2,1,-7},
		{0,2,1},
		{1,6,8},
	}))
	fmt.Println(Solution([][]int{{0,0,0},{0,0,0},{0,0,0}}))
	fmt.Println(Solution([][]int{{0,0},{0,0},{0,0}}))
	fmt.Println(Solution([][]int{}))
	fmt.Println(Solution([][]int{{2}}))
	fmt.Println(Solution([][]int{{1},{2}}))
	fmt.Println(Solution([][]int{{0,0}}))
	fmt.Println(Solution([][]int{{0,0},{0,0}}))
	fmt.Println(Solution([][]int{{1,0,-1},{1,0,2},{1,0,-1}}))
}
