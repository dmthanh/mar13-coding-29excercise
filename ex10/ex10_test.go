package ex10

import (
	"fmt"
	"testing"
)

func TestEx10(t *testing.T) {
	var result int

	var testcase [][][]int
	var answer []int

	testcase = append(testcase, [][]int{
		[]int{2, 7, 5},
		[]int{3, 1, 1},
		[]int{2, 1, -7},
		[]int{0, 2, 1},
		[]int{1, 6, 8},
	})
	answer = append(answer, 2)

	testcase = append(testcase, [][]int{
		[]int{0, 0, 0},
		[]int{0, 0, 0},
		[]int{0, 1, 0},
		[]int{0, 0, 0},
		[]int{0, 0, 0},
	})
	answer = append(answer, 1)
	testcase = append(testcase, [][]int{{0,0,0},{0,0,0},{0,0,0}})
	answer = append(answer, 1)
	testcase = append(testcase, [][]int{{0,0},{0,0},{0,0}})
	answer = append(answer, 1)
	testcase = append(testcase, [][]int{})
	answer = append(answer, 1)
	testcase = append(testcase, [][]int{{2}})
	answer = append(answer, 1)
	testcase = append(testcase, [][]int{{1},{2}})
	answer = append(answer, 1)
	testcase = append(testcase, [][]int{{0,0}})
	answer = append(answer, 1)
	testcase = append(testcase, [][]int{{0,0},{0,0}})
	answer = append(answer, 1)
	testcase = append(testcase, [][]int{{1,0,-1},{1,0,2},{1,0,-1}})
	answer = append(answer, 1)

	for i := range answer {
		result = Ex10(testcase[i])
		res:=Solution(testcase[i])
		//if result != answer[i] {
		//	t.Error("Fail case/correct/your answer", testcase[i], answer[i], result)
		//} else {
		//	fmt.Println("test case: ", testcase[i])
		//	fmt.Println("answer: ", answer[i])
		//	fmt.Println("result: ", result)
		//}
		fmt.Println(result," ",res)
	}
}
