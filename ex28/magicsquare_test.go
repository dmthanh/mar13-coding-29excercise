package ex28

import (
	"testing"
)

func TestSolution(t *testing.T) {
	var testcase [][][]int
	var answer []int
	testcase = append(testcase, [][]int{
		[]int{4, 3, 4, 5, 3},
		[]int{2, 7, 3, 8, 4},
		[]int{1, 7, 6, 5, 2},
		[]int{8, 4, 9, 5, 5},
	})
	answer = append(answer, 3)

	testcase = append(testcase, [][]int{
		[]int{2, 2, 1, 1},
		[]int{2, 2, 2, 2},
		[]int{1, 2, 2, 2},
	})
	answer = append(answer, 2)

	testcase = append(testcase, [][]int{
		[]int{7, 2, 4},
		[]int{2, 7, 6},
		[]int{9, 5, 1},
		[]int{4, 3, 8},
		[]int{3, 5, 4},
	})
	answer = append(answer, 3)

	for i := range testcase {
		result := Solution(testcase[i])
		if result != answer[i] {
			t.Error(testcase[i], result, answer[i])
		}

	}
}
