package ex28

func MaxMagicSquare(a [][]int) int {
	sum := 0
	for i := 0; i < len(a); i++ {
		sum += a[i][0]
	}
	cross1 := 0
	cross2 := 0
	for i := 0; i < len(a); i++ {
		row := 0
		col := 0
		cross1 += a[i][i]
		cross2 += a[i][len(a)-i-1]
		for j := 0; j < len(a); j++ {
			row += a[i][j]
			col += a[j][i]
		}
		if row != sum || col != sum {
			return 1
		}
	}
	if cross1 != sum || cross2 != sum {
		return 1
	}
	return len(a)
}

func Solution(a [][]int) int {
	row := len(a)
	col := len(a[0])
	maxSize := 1
	for i := 0; i < (row - 2); i++ {
		for j := 0; j < (col - 2); j++ {
			maxStep := row - i
			if maxStep > col-j {
				maxStep = col - j
			}
			for k := maxStep; k >= 2; k-- {
				var b [][]int
				for k1 := i; k1 < i+k; k1++ {
					b = append(b, a[k1][j:j+k])
				}
				tmp := MaxMagicSquare(b)
				if tmp > maxSize {
					maxSize = tmp
					break
				}

			}

		}
	}
	return maxSize

}
