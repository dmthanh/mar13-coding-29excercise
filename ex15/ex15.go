package ex15

func Ex15(a int) bool {
	var end []int
	var b int
	if a < 0 {
		return false
	} else {
		b = a
	}
	for b > 0 {
		end = append(end, b%10)
		b = b / 10
	}
	b = len(end)
	for i := 0; i < b/2; i++ {
		if end[i] != end[b-i-1] {
			return false
		}
	}
	return true
}
