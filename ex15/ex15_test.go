package ex15

import (
	"fmt"
	"testing"
)

//excercise 1 in file #2
func TestEx15(t *testing.T) {
	var result bool
	var testcase []int
	var answer []bool

	testcase = append(testcase, 121)
	answer = append(answer, true)
	testcase = append(testcase, -121)
	answer = append(answer, false)
	testcase = append(testcase, 10)
	answer = append(answer, false)
	testcase = append(testcase, 0)
	answer = append(answer, true)
	testcase = append(testcase, 1)
	answer = append(answer, true)
	testcase = append(testcase, 123)
	answer = append(answer, false)

	for i := range answer {
		result = Ex15(testcase[i])
		if result != answer[i] {
			t.Error("Fail case/correct/your answer", testcase[i], answer[i], result)
		} else {
			fmt.Println("test case: ", testcase[i])
			fmt.Println("answer: ", answer[i])
			fmt.Println("result: ", result)
		}
	}
}
