package ex24

import (
	"fmt"
	"testing"
)

func TestString(t *testing.T) {
	var testcase [][]int
	var answer []string
	var result string

	testcase = append(testcase, []int{6, 1,  1})
	answer = append(answer, "aabaacaa")
	testcase = append(testcase, []int{1, 3, 1})
	answer = append(answer, "bbacb")
	testcase = append(testcase, []int{0, 1, 8})
	answer = append(answer, "ccbcc")
	testcase = append(testcase, []int{2, 2, 30})
	answer = append(answer, "ccaccbccaccbcc")
	testcase = append(testcase, []int{0, 0, 3})
	answer = append(answer, "cc")
	testcase = append(testcase, []int{3, 3, 30})
	answer = append(answer, "cc")
	testcase = append(testcase, []int{3, 3, 8})
	answer = append(answer, "cc")
	testcase = append(testcase, []int{3, 3, 3})
	answer = append(answer, "cc")
	testcase = append(testcase, []int{4, 5, 6})
	answer = append(answer, "cc")
	testcase = append(testcase, []int{2, 5, 6})
	answer = append(answer, "cc")
	testcase = append(testcase, []int{2, 3, 6})
	answer = append(answer, "cc")
	testcase = append(testcase, []int{0, 3, 6})
	answer = append(answer, "cc")
	testcase = append(testcase, []int{0, 0, 6})
	answer = append(answer, "cc")
	testcase = append(testcase, []int{4, 6, 10})
	answer = append(answer, "cc")
	testcase = append(testcase, []int{2, 2, 9})
	answer = append(answer, "cc")
	testcase = append(testcase, []int{3, 3, 13})
	answer = append(answer, "cc")

	for i := range answer {
		result = Solution(testcase[i][0], testcase[i][1], testcase[i][2])
		str:=NewSolution(testcase[i][0], testcase[i][1], testcase[i][2])
		//str2:=thanh(testcase[i][0], testcase[i][1], testcase[i][2])
		if len(str)!=len(str){
			fmt.Println(result, " ",len(result)," ",str," ",len(str))
		}

	}
}

