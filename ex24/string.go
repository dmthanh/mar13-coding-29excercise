package ex24

import (
	"sort"
)

func Abs(a int) int {
	if a < 0 {
		return -a
	} else {
		return a
	}
}

func Solution(a int, b int, c int) string {
	type myType struct {
		Character  string
		Value      int
		singleStep int
	}

	myVar := []myType{
		myType{"a", a, 0},
		myType{"b", b, 0},
		myType{"c", c, 0},
	}
	sort.Slice(myVar[:], func(i, j int) bool {
		return myVar[i].Value > myVar[j].Value
	})
	for i := 1; i < len(myVar); i++ {
		myVar[i].singleStep = myVar[i-1].Value - myVar[i].Value + myVar[i-1].singleStep
	}
	lastChar := ""
	result := ""
	for {
		if myVar[0].Character == lastChar || myVar[0].Value == 0 {
			return result
		}
		if myVar[0].Value >= 2 && myVar[0].singleStep <= 0 {
			result = result + myVar[0].Character + myVar[0].Character
			myVar[0].Value = (myVar[0].Value - 2) * (-1)
			lastChar = myVar[0].Character
		} else {
			result = result + myVar[0].Character
			myVar[0].Value = (myVar[0].Value - 1) * (-1)
			lastChar = myVar[0].Character
			myVar[0].singleStep -= 1
		}
		myVar[1].Value = Abs(myVar[1].Value)
		myVar[2].Value = Abs(myVar[2].Value)
		sort.Slice(myVar[:], func(i, j int) bool {
			return myVar[i].Value > myVar[j].Value
		})

	}
	return ""

}
func InsertAfterIndex (index int, letter string, old string) string {
	return old[:index+1]+letter+old[index+1:]
}
func LongestResult (s string) string{
	for i:=2; i<len(s); i++{
		if s[i-1]==s[i-2] && s[i]==s[i-1]{
			return s[:i]
		}
	}
	return s

}
func InsertToString(step int, letter, old string) string{
	str:=""
	for i:=range old{
		if i%step==0{
			str+=letter+string(old[i])
		}
		str+=string(old[i])
	}
	return str
}
type One struct{
	Letter string
	Number int
}

type ByNumber []One

func (a ByNumber) Len() int {
	return len(a)
}

func (a ByNumber) Less(i, j int) bool {
	return a[i].Number > a[j].Number

}

func (a ByNumber) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}

func MakeString(x,k int, isBigger bool, one []One) string{
	tmp:=[]string{}
	str:=""
	if isBigger{
		for i:=1; i<len(one); i++{
			for j:=0; j<one[i].Number; j++{
				tmp=append(tmp,one[i].Letter)
			}

		}
		now:=len(tmp)-1
		no2:=one[0].Number
		for i:=0; i<x; i++{
			if now<0 || no2<2{
				break
			}
			str+=one[0].Letter+one[0].Letter+tmp[now]
			now-=1
			no2-=2
		}
		for i:=0; i<k; i++{
			if now<0 || no2<1 {break}
			str+=one[0].Letter+tmp[now]
			now-=1
			no2-=1
		}
		if no2>2{
			no2=2
		}
		for i:=0; i<no2; i++{
			str+=one[0].Letter
		}
	} else {
		now:=0
		for now<one[1].Number || now <one[2].Number{
			if now <one[1].Number{
				tmp=append(tmp,one[1].Letter)
			}
			if now<one[2].Number{
				tmp=append(tmp, one[2].Letter)
			}
			now+=1
		}
		now=len(tmp)-1
		no2:=one[0].Number
		for i:=0; i<x; i++{
			if now<1 || no2<1 {break}
			str+=tmp[now]+tmp[now-1]+one[0].Letter
			now-=2
			no2-=1
		}
		for i:=0; i<k; i++{
			if now<0 || no2<1{ break}
			str+=tmp[now]+one[0].Letter
			now-=1
			no2-=1
		}

	}
	return str
}

func NewSolution(a int, b int, c int) string {
	x:=[]One{One{"a",a},One{"b",b},One{"c",c}}
	sort.Sort(ByNumber(x))
	c=x[0].Number
	b=x[1].Number
	a=x[2].Number
	if c >= a+b{
		return MakeString(c-a-b,2*(a+b)-c,true,x)

	} else {
		return MakeString(a+b-c,2*c-(a+b),false,x)
	}

}

//func thanh(a,b,c int) string {
//	x:= []One{One{"a",a},One{"b",b},One{"c",c}}
//	min1:=2
//	left := 2
//	str:=""
//	for min1!=0{
//		if x[min1].Number == 0 {
//			min1 -= 1
//		}
//		sort.Sort(ByNumber(x))
//		if x[0].Number < 2 {
//			left = x[0].Number
//		}
//		for i := 0; i < left; i++ {
//			str += x[0].Letter
//		}
//		x[0].Number -= left
//		if min1!=0 {
//			if x[min1].Number > 0 && x[min1].Letter!=string(str[len(str)]) {
//				str += x[min1].Letter
//				x[min1].Number -= 1
//			}
//
//		}
//	}
//	return str
//}
