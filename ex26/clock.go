package ex26

import (
	"strconv"
	"strings"
)

func convertToString (x int) string{
	str1:= strconv.Itoa(x)
	if x<10 {
		str1= "0"+str1
	}
	return str1
}
func convertToNumber (str1 string) int{
	x,_:= strconv.Atoi(str1)
	return x
}
func isInteresting (x int, y int, z int) bool{
	a:=make(map[string]bool)
	str1:= convertToString(x)+convertToString(y)+convertToString(z)
	for i:=range str1{
		a[string(str1[i])]=true
	}
	if len(a)>2{
		return false
	}
	return true
}
func IsFrSmallerThanTo (a,b []int) bool{
	if a[0]<b[0]{
		return true
	}
	if a[0]>b[0]{
		return false
	}
	if a[1]<b[1]{
		return true
	}
	if a[1]>b[1]{
		return false
	}
	if a[2]<b[2]{
		return true
	}
	if a[2]>b[2]{
		return false
	}
	return true
}
func Solution(fr string, to string ) int {
	tmp:= strings.Split(fr,":")
	var fr1, to1 []int
	for _,v:=range tmp{
		fr1=append(fr1,convertToNumber(v))
	}
	tmp= strings.Split(to,":")
	for _,v:=range tmp{
		to1=append(to1,convertToNumber(v))
	}
	return solve (fr1,to1)
}
func solve(a,b []int) int{
	var min1, min0, sec0, sec1 int
	count:=0
	for h:=a[0]; h<=b[0]; h++{
		if h==b[0]{
			min1=b[1]
		} else{
			min1=59
		}
		if h==a[0]{
			min0=a[1]
		} else {
			min0=0
		}
		for m:=min0; m<=min1; m++{
			if h==a[0] && m==a[1] {
				sec0=a[2]
			} else {
				sec0=0
			}
			if h==b[0] && m==b[1]{
				sec1=b[2]
			} else {
				sec1=59
			}
			for s:=sec0; s<=sec1; s++{
				if isInteresting(h,m,s){
					count+=1
				}
			}
		}
	}
	return count
}
