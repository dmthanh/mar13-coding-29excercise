package ex26

import (
	"testing"
)

func TestSolution(t *testing.T) {
	type input struct {
		fr string
		to string
	}
	var testcase []input
	var answer []int
	testcase=append(testcase,input{"15:15:00","15:15:12"})
	answer=append(answer,1)
	testcase=append(testcase,input{"22:22:21","22:22:23"})
	answer=append(answer,3)
	testcase=append(testcase,input{"23:33:32","23:59:59"})
	answer=append(answer,2)
	testcase=append(testcase,input{"00:00:00","23:59:59"})
	answer=append(answer,504)





	for i := range testcase {
		result:=Solution(testcase[i].fr,testcase[i].to)
		if result!=answer[i]{
			t.Error(testcase[i].fr,testcase[i].to,result,answer[i])
		}


	}
}
