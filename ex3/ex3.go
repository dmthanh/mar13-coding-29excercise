package ex3

import (
	"strconv"
	"strings"
)

func ToInt(s string) int {
	i, err := strconv.Atoi(s)
	if err == nil {
		return i
	} else {
		return 0
	}
}

func Ex3(s string) int {
	rawLines := strings.Split(s, "\n")
	splitLines := make([][]string, 0)
	for _, line := range rawLines {
		line = strings.ReplaceAll(line, ":", " ")
		line = strings.ReplaceAll(line, "-", " ")
		splitLines = append(splitLines, strings.Split(line, " "))
	}
	dayToInt := map[string]int{
		"Mon": 0,
		"Tue": 1,
		"Wed": 2,
		"Thu": 3,
		"Fri": 4,
		"Sat": 5,
		"Sun": 6,
	}
	var aWeek [7][24][60]int
	var day, hour1, hour2, min1, min2 int
	for _, line := range splitLines {
		day = dayToInt[line[0]]
		hour1 = ToInt(line[1])
		hour2 = ToInt(line[3])
		min1 = ToInt(line[2])
		min2 = ToInt(line[4])
		for hour := hour1; hour <= hour2; hour++ {
			if hour1 == hour2 {
				for min := min1; min < min2; min++ {
					aWeek[day][hour][min] = 1
				}
			} else {
				if hour == hour1 {
					for min := min1; min < 60; min++ {
						aWeek[day][hour][min] = 1
					}
				} else {
					if hour == hour2 {
						for min := 0; min < min2; min++ {
							aWeek[day][hour][min] = 1
						}
					} else {
						for min := 0; min < 60; min++ {
							aWeek[day][hour][min] = 1
						}
					}
				}
			}
		}
	}
	count := 0
	max := 0
	for day := 0; day < 7; day++ {
		for hour := 0; hour < 24; hour++ {
			for min := 0; min < 60; min++ {
				if aWeek[day][hour][min] != 1 {
					count++
				} else {
					if count > max {
						max = count
					}
					count = 0
				}
			}
		}
	}
	if count > max {
		max = count
	}
	return max
}
