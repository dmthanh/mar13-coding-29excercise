package ex8

func Ex8(a []int) int {
	needOn := 1
	b := make([]int, len(a))
	maxWait := -1
	count := 0
	var connect bool
	var maxConnect int
	for i := 0; i < len(a); i++ {

		//fmt.Println("needOn: ", needOn)
		//fmt.Println("a[i]: ", a[i])
		if a[i] == needOn {
			b[needOn-1] = 1

			connect = true
			for j := needOn - 1; j < maxWait; j++ {

				if b[j] != 1 {
					connect = false
					maxConnect = j
					break
				}
			}
			//fmt.Println("connect: ", connect)
			if connect {
				if maxWait > needOn {
					needOn = maxWait + 1
				} else {
					needOn++
				}
				count++
			} else {
				needOn = maxConnect + 1
			}

		} else {
			b[a[i]-1] = 1
			if a[i] > maxWait {
				maxWait = a[i]
			}

		}
		// fmt.Println("needOn: ", needOn)
		// fmt.Println("maxwait: ", maxWait)
		//fmt.Println(b)

	}
	return count
}
