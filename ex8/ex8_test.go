package ex8

import (
	"fmt"
	"testing"
)

func TestEx8(t *testing.T) {
	var testcase [][]int
	var answer []int
	testcase = append(testcase, []int{
		2, 1, 3, 5, 4})
	answer = append(answer, 3)

	testcase = append(testcase, []int{
		2, 3, 4, 1, 5})
	answer = append(answer, 2)

	testcase = append(testcase, []int{
		1, 3, 4, 2, 5})
	answer = append(answer, 3)
	var result int
	for i := range answer {
		result = Ex8(testcase[i])
		if result != answer[i] {
			t.Error("Fail case/correct/your answer", testcase[i], answer[i], result)
		} else {
			fmt.Println("test case: ", testcase[i])
			fmt.Println("answer: ", answer[i])
			fmt.Println("result: ", result)
		}
	}
}
