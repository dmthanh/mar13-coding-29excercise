package ex21

import (
	"fmt"
	"testing"
)

func TestEx21(t *testing.T) {
	var result int
	var testcase [][]int
	var answer []int

	testcase = append(testcase, []int{-1, 1, 3, 3, 3, 2, 3, 2, 1, 0})
	answer = append(answer, 5)
	testcase = append(testcase, []int{0,1,2,3,4})
	answer = append(answer, 6)
	testcase = append(testcase, []int{0,0,0,3,4})
	answer = append(answer, 1)
	testcase = append(testcase, []int{0,0,0,0,4})
	answer = append(answer, 3)

	for i := range answer {
		result = Ex21(testcase[i])
		if result != answer[i] {
			t.Error("Fail case/correct/your answer", testcase[i], answer[i], result)
		}  else {
			fmt.Println("test case: ", testcase[i])
			fmt.Println("answer: ", answer[i])
			fmt.Println("result: ", result)
		}
	}
}
