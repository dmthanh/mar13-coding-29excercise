package ex21

func IsStable(a []int) bool {
	step := a[1] - a[0]
	for i := 1; i < len(a)-1; i++ {
		if a[i+1]-a[i] != step {
			return false
		}
	}
	return true
}
func Ex21(a []int) int {
	result := 0
	for i := 0; i < len(a)-2; i++ {
		for j := i + 2; j < len(a); j++ {
			if IsStable(a[i : j+1]) {
				result++
			}
		}
	}
	return result
}
