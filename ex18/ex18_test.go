package ex18

import (
	"fmt"
	"testing"
)

func sliceCompare(a, b []int) bool {
	if len(a) != len(b) {
		return false
	}
	for i := range a {
		if a[i] != b[i] {
			return false
		}
	}
	return true
}
func TestEx18(t *testing.T) {
	var result []int
	var testcase [][][]int
	var answer [][]int

	testcase = append(testcase, [][]int{
		[]int{2, 4, 3},
		[]int{5, 6, 4},
	})
	answer = append(answer, []int{7, 0, 8})

	testcase = append(testcase, [][]int{
		[]int{1, 1, 1},
		[]int{2, 2, 2},
	})
	answer = append(answer, []int{3, 3, 3})

	testcase = append(testcase, [][]int{
		[]int{1, 0, 0},
		[]int{2, 0, 0},
	})
	answer = append(answer, []int{3})

	for i := range answer {
		result = Ex18(testcase[i][0], testcase[i][1])
		if !sliceCompare(result, answer[i]) {
			t.Error("Fail case/correct/your answer", testcase[i], answer[i], result)
		}  else {
			fmt.Println("test case: ", testcase[i])
			fmt.Println("answer: ", answer[i])
			fmt.Println("result: ", result)
		}
	}
}
