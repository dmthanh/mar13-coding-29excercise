package ex18

import (
	"math"
)

type node struct {
	value int
	next  *node
}

func linkedListToInt(head *node) int {
	tmp := 0
	i := 0
	for head.next != nil {
		tmp += head.value * int(math.Pow10(i))
		i++
		head = head.next
	}
	tmp += head.value * int(math.Pow10(i))
	return tmp
}

func sliceToLinkedList(a []int) *node {
	var listNode []*node
	for i := range a {
		listNode = append(listNode, &node{a[i], nil})
	}
	for i := 0; i < len(listNode)-1; i++ {
		listNode[i].next = listNode[i+1]
	}
	return listNode[0]
}

func intToSliceReverse(a int) []int {
	var tmp []int
	for a > 0 {
		tmp = append(tmp, a%10)
		a = a / 10
	}
	return tmp
}

func Ex18(a, b []int) []int {

	headA := sliceToLinkedList(a)
	headB := sliceToLinkedList(b)

	intA := linkedListToInt(headA)
	intB := linkedListToInt(headB)

	intA += intB

	return intToSliceReverse(intA)

}
