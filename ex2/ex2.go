package ex2

func IsIncrease(a []int, k int) bool {
	if k > 0 && k < len(a)-1 {
		if a[k-1] > a[k+1] {
			return false
		}
	}
	for i := 0; i < k-1; i++ {
		if a[i] > a[i+1] {
			return false
		}
	}
	for i := k + 1; i < len(a)-1; i++ {
		if a[i] > a[i+1] {
			return false
		}
	}
	return true
}
func Ex2(a []int) int {
	count := 0
	for i := range a {

		if IsIncrease(a, i) {
			count++
		}
	}
	return count
}
