package ex2

import (
	"fmt"
	"testing"
)

func TestEx2(t *testing.T) {
	testcase := make([][]int, 0)
	answer := make([]int, 0)
	testcase = append(testcase, []int{3, 4, 5, 4})
	answer = append(answer, 2)
	testcase = append(testcase, []int{4, 5, 2, 3, 4})
	answer = append(answer, 0)
	testcase = append(testcase, []int{1, 2, 3, 4, 5, 6, 7})
	answer = append(answer, 7)
	testcase = append(testcase, []int{2, 3, 1, 4, 5, 1, 6})
	answer = append(answer, 0)
	testcase = append(testcase, []int{1, 2, 3, 4, 3, 4})
	answer = append(answer, 2)
	//var tmp []int
	// k := 100000
	// tmp := make([]int, k)

	// // for i := 0; i < k; i++ {
	// // 	tmp = append(tmp, i+1)
	// // }
	// testcase = append(testcase, tmp)
	// answer = append(answer, k)
	for i := range testcase {
		if Ex2(testcase[i]) != answer[i] {
			t.Error("Ex2 fail - testcase - correct answer - your anwser", testcase[i], answer[i], Ex2(testcase[i]))
		} else {
			fmt.Println("test case: ", testcase[i])
			fmt.Println("answer: ", answer[i])
		}
	}
}
