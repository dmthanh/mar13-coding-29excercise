package ex11

import (
	"sort"
)

func compareSlice(a, b []int) bool {
	for i, v := range a {
		if v != b[i] {
			return false
		}
	}
	return true
}
func Ex11(a []int) bool {
	b := make([]int, len(a))
	_ = copy(b, a)
	sort.Ints(b)
	for i := 0; i < len(a)-1; i++ {
		if a[i] != b[i] {
			for j := i + 1; j < len(b); j++ {
				if a[i] == b[j] && a[i] != a[j] {
					a[i] = a[j]
					a[j] = b[j]
					break
				}
			}
			if compareSlice(a, b) {
				return true
			} else {
				return false
			}
		}
	}
	return true
}
