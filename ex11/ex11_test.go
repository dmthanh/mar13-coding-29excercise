package ex11

import (
	"fmt"
	"testing"
)

func TestEx11(t *testing.T) {
	var result bool

	var testcase [][]int
	var answer []bool

	testcase = append(testcase, []int{
		1, 5, 3, 3, 7,
	})
	answer = append(answer, true)

	testcase = append(testcase, []int{
		1, 3, 5, 3, 4,
	})
	answer = append(answer, false)

	testcase = append(testcase, []int{
		1, 3, 5,
	})
	answer = append(answer, true)

	testcase = append(testcase, []int{
		1, 3, 5, 9, 7,
	})
	answer = append(answer, true)

	testcase = append(testcase, []int{
		1, 9, 5, 6, 7,
	})
	answer = append(answer, false)

	for i := range answer {
		result = Ex11(testcase[i])
		if result != answer[i] {
			t.Error("Fail case/correct/your answer", testcase[i], answer[i], result)
		} else {
			fmt.Println("test case: ", testcase[i])
			fmt.Println("answer: ", answer[i])
			fmt.Println("result: ", result)
		}
	}
}
