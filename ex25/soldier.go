package soldider

func Solution(a []int ) int {
	b:= make(map[int]bool)
	for _,v:=range a{
		b[v]=true
	}
	count:=0
	for _,v:=range a{
		if b[v+1]{
			count+=1
		}
	}
	return count
}
