package ex7

func Ex7(s, t string) string {
	if s == t {
		return "NOTHING"
	}
	if len(t)-len(s) > 1 || len(s) > len(t) {
		return "IMPOSSIBLE"
	}
	if s == t[:len(t)-1] {
		return "ADD " + string(t[len(t)-1])
	}
	count := 0
	var s1, t1 byte
	var badIndex int
	for i := 0; i < len(t); i++ {
		if s[i] != t[i] {
			count++
			if count == 1 {
				s1 = s[i]
				t1 = t[i]
				badIndex = i
			}
		}
	}
	if count == 1 {
		return "CHANGE " + string(s1) + " " + string(t1)
	}
	for i := badIndex + 1; i < len(t); i++ {
		if t[i] == s1 {
			if i < len(t)-1 {
				if s[i+1] != s1 {
					tmp := s[:badIndex] + s[badIndex+1:i+1] + string(s1) + s[i+1:]
					if tmp == t {
						return "MOVE " + string(s1)
					}

				}
			} else {
				tmp := s[:badIndex] + s[badIndex+1:] + string(s1)
				if tmp == t {
					return "MOVE " + string(s1)
				}

			}
		}
	}
	return "IMPOSSIBLE"
}
