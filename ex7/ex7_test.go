package ex7

import (
	"fmt"
	"testing"
)

func TestEx7(t *testing.T) {
	testcase := make([][]string, 0)
	answer := make([]string, 0)

	testcase = append(testcase, []string{
		"nice",
		"nicer"})
	answer = append(answer, "ADD r")

	testcase = append(testcase, []string{
		"test",
		"tent"})
	answer = append(answer, "CHANGE s n")

	testcase = append(testcase, []string{
		"beans",
		"banes"})
	answer = append(answer, "MOVE e")

	testcase = append(testcase, []string{
		"abcd",
		"abdc"})
	answer = append(answer, "MOVE c")

	testcase = append(testcase, []string{
		"o",
		"odd"})
	answer = append(answer, "IMPOSSIBLE")

	testcase = append(testcase, []string{
		"aibcdidfg",
		"abcdidifg"})
	answer = append(answer, "MOVE i")

	testcase = append(testcase, []string{
		"agbcdidif",
		"abcdidifg"})
	answer = append(answer, "MOVE g")
	var result string
	for i := range answer {
		result = Ex7(testcase[i][0], testcase[i][1])
		if result != answer[i] {
			t.Error("Fail case/correct/your answer", testcase[i], answer[i], result)
		} else {
			fmt.Println("test case: ", testcase[i])
			fmt.Println("answer: ", answer[i])
			fmt.Println("result: ", result)
		}

	}
}
