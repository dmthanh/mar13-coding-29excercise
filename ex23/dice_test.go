package ex23

import (
	"testing"
)

func TestDice(t *testing.T) {
	var testcase [][]int
	var answer []int
	var result int

	testcase = append(testcase, []int{1, 2, 3})
	answer = append(answer, 2)
	testcase = append(testcase, []int{1, 1, 6})
	answer = append(answer, 2)
	testcase = append(testcase, []int{1, 6, 2, 3})
	answer = append(answer, 3)
	testcase = append(testcase, []int{1, 1, 1, 1})
	answer = append(answer, 0)
	testcase = append(testcase, []int{1, 1, 1, 6})
	answer = append(answer, 2)

	for i := range answer {
		result = Solution(testcase[i])
		if result != answer[i] {
			t.Error(testcase, answer[i], result)
		}
	}
}
