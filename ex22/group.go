package ex22

import (
	"fmt"
	"strconv"
)
func standardizeNumber(x int, maxLen int) string{
	str1:= strconv.Itoa(x)
	for len(str1)<maxLen{
		str1= " "+str1
	}
	return str1
}
func makeALine(a []int, maxLen int) string{
	str1:="|"
	for _, v:= range a{
		str1+=standardizeNumber(v, maxLen)+"|"
	}
	str1+="\n"
	return str1
}
func makeABreak(breakLen int, maxLen int) string{

	oneCell:=""
	for i:=0; i<maxLen; i++{
		oneCell+="-"
	}
	oneCell+="+"
	str1:= "+"
	for i:=0; i<breakLen; i++{
		str1+=oneCell
	}
	str1+="\n"
	return str1
}
func Solution(a []int, k int) {
	max1:=a[0]
	for _,v:=range a{
		if v>max1{
			max1=v
		}
	}
	strMax1:= strconv.Itoa(max1)
	maxLen:= len(strMax1)
	str1:=""
	if len(a)<k{
		str1=makeABreak(len(a),maxLen)
	} else {
		str1=makeABreak(k,maxLen)
	}
	tmp:=0
	for tmp< len(a){
		if tmp+k-1 <len(a){
			str1+=makeALine(a[tmp:tmp+k],maxLen)
			str1+=makeABreak(k,maxLen)

		} else {
			str1+=makeALine(a[tmp:],maxLen)
			str1+=makeABreak(len(a)-tmp,maxLen)
		}
		tmp+=k
	}
	fmt.Print(str1)
	//return str1
}
