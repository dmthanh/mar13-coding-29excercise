package ex13

import (
	"fmt"
	"testing"
)

//excercise 1 in file #2
func TestEx13(t *testing.T) {
	var result []int
	type input struct {
		t int
		a []int
	}
	var testcase []input
	var answer [][]int

	testcase = append(testcase, input{9,
		[]int{2, 7, 11, 15},
	})
	answer = append(answer, []int{0, 1})

	testcase = append(testcase, input{0,
		[]int{0, 0, 0, 0},
	})
	answer = append(answer, []int{0, 1, 2, 3})

	testcase = append(testcase, input{3,
		[]int{0, 0, 1, 2},
	})
	answer = append(answer, []int{2, 3})

	testcase = append(testcase, input{8,
		[]int{0, 7, 1, 7},
	})
	answer = append(answer, []int{1, 2})


	for i := range answer {
		result = Ex13(testcase[i].t, testcase[i].a)
		fmt.Println("test case: ", testcase[i])
		fmt.Println("answer: ", answer[i])
		fmt.Println("result: ", result)
		if result[0] != answer[i][0] || result[1] != answer[i][1] {
			t.Error("Fail case/correct/your answer", testcase[i], answer[i], result)
		}
	}
}

