package ex13

func Ex13(t int, a []int) []int {
	for i := 0; i < len(a)-1; i++ {
		for j := i + 1; j < len(a); j++ {
			if a[i]+a[j] == t {
				return []int{i, j}
			}
		}
	}
	return []int{0, 0}
}
