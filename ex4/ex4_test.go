package ex4

import (
	"fmt"
	"testing"
)

func TestEx4(t *testing.T) {
	var testcase [][]string
	var result int
	var answer []int
	testcase = append(testcase, []string{
		"..X...",
		"......",
		"....X.",
		".X....",
		"..X.X.",
		"...O..",
	})
	answer = append(answer, 2)

	testcase = append(testcase, []string{
		"X....",
		".X...",
		"..O..",
		"...X.",
		".....",
	})
	answer = append(answer, 0)

	for i := range testcase {
		result = Ex4(testcase[i])
		if result != answer[i] {
			t.Error("case fail/testcase/correct answer/your anwser", testcase[i], answer[i], result)
		}  else {
			fmt.Println("test case: ", testcase[i])
			fmt.Println("answer: ", answer[i])
		}
	}

}
