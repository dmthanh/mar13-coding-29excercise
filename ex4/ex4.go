package ex4

import (
	"fmt"
	"strings"
)

func max(a, b int) int {
	if a > b {
		return a
	} else {
		return b
	}
}

func MoveOn(a []string, i, j, result int) int {
	//hit the wall
	if i == 0 {
		return result
	}
	maxResult := result

	if j-1 >= 0 {
		//upper left is clear
		if a[i-1][j-1] == '.' {
			maxResult = max(MoveOn(a, i-1, j-1, result), maxResult)
		}
		//upper left can beat
		if a[i-1][j-1] == 'X' && i-2 >= 0 && j-2 >= 0 {
			if a[i-2][j-2] == '.' {
				maxResult = max(MoveOn(a, i-2, j-2, result+1), maxResult)
			}
		}

	}

	if j+1 < len(a[0]) {
		//upper right is clear
		if a[i-1][j+1] == '.' {
			maxResult = max(MoveOn(a, i-1, j+1, result), maxResult)
		}
		//upper right can beat
		if a[i-1][j+1] == 'X' && i-2 >= 0 && j+2 < len(a[0]) {
			if a[i-2][j+2] == '.' {
				maxResult = max(MoveOn(a, i-2, j+2, result+1), maxResult)
			}
		}
	}

	return maxResult

}

func findO(a []string) (int, int) {
	var j int
	for i := len(a) - 1; i > -1; i-- {
		j = strings.Index(a[i], "O")
		if j != -1 {
			return i, j
		}
	}
	return 0, 0
}

func Ex4(a []string) int {
	x, y := findO(a)
	fmt.Println(x, y)
	return MoveOn(a, x, y, 0)
}
