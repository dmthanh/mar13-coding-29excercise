package ex19

import (
	"fmt"
	"testing"
)

func TestEx19(t *testing.T) {
	var result int
	var testcase []string
	var answer []int

	testcase = append(testcase, "abcabcbb")
	answer = append(answer, 3)
	testcase = append(testcase, "bbbbb")
	answer = append(answer, 1)
	testcase = append(testcase, "pwwkew")
	answer = append(answer, 3)
	testcase = append(testcase, "abcd")
	answer = append(answer, 4)
	testcase = append(testcase, "abcdaabb")
	answer = append(answer, 4)

	for i := range answer {
		result = Ex19(testcase[i])
		if result != answer[i] {
			t.Error("Fail case/correct/your answer", testcase[i], answer[i], result)
		} else {
			fmt.Println("test case: ", testcase[i])
			fmt.Println("answer: ", answer[i])
			fmt.Println("result: ", result)
		}
	}
}
