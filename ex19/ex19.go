package ex19

import "strings"

func Ex19(s string) int {
	maxLen := 0
	var tmp string
	for i := 0; i < len(s)-1; i++ {
		tmp = string(s[i])
		for j := i + 1; j < len(s); j++ {
			if strings.Contains(tmp, string(s[j])) {
				break
			}
			tmp += string(s[j])

		}
		if len(tmp) > maxLen {
			maxLen = len(tmp)
		}

	}
	return maxLen
}
