package ex1

import (
	"strings"
)

func Solution(s string) string {
	stopWord := []string{"-", " "}
	for _, w := range stopWord {
		s = strings.ReplaceAll(s, w, "")
	}
	var s2 string
	for i := 0; i < len(s); i++ {
		s2 = s2 + string(s[i])
		if (i+1)%3 == 0 && i<len(s)-1 {
			s2 = s2 + "-"
		}
	}
	s=s2
	now:= len(s)-1
	for string(s[now])!="-"{
		now-=1
	}
	if len(s)-now<3{
		s= s[:now-1]+"-"+string(s[now-1])+string(s[len(s)-1])
	}
	return s
}
