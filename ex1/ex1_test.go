package ex1

import (
	"fmt"
	"testing"
)

func TestPhoneNumber(t *testing.T) {
	var testcase []string
	var answer []string

	testcase = append(testcase, "00-44241-131-1-3-3216-032-     -1131 --11361-")
	answer = append(answer, "004-424-113-113-321-603-211-311-13-61")
	testcase = append(testcase, "00-44  48  5555  8361")
	answer = append(answer,"004-448-555-583-61" )
	testcase = append(testcase, "0  -  22  1985--324")
	answer = append(answer, "022-198-53-24")
	testcase = append(testcase, "555372654")
	answer = append(answer, "555-372-654")

	for i := range answer {
		fmt.Println("test case: ", testcase[i])
		fmt.Println("answer: ", answer[i])
		result := Solution(testcase[i])
		if result != answer[i] {
			t.Error(testcase[i]," : ", answer[i]," ",result)
		}
	}
}
